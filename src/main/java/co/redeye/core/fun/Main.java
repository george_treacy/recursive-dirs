package co.redeye.core.fun;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.counting;

import static org.junit.Assert.fail;

/**
 * Cool Beans
 * Created by george on 20/09/15.
 */
public class Main {

    public static void main(String[] args) {

        final String path = "/home/george/Downloads";
        //final String path = "/media/george/Seagate Backup Plus Drive/QUU First Set 150915";
        final Path startPath = Paths.get(path).normalize().toAbsolutePath();
        try {
            Map<Integer, Long> stats = Files.walk(startPath)
                    .sequential()
                    .collect(groupingBy(n -> Files.isDirectory(n, LinkOption.NOFOLLOW_LINKS) ? 1 : 2, counting()));
            System.out.format("Files: %d, dirs: %d. ", stats.get(2), stats.get(1));
        } catch (IOException ex) {
            fail(ex.getMessage());
        }
        System.out.println("Boo...");
    }

}
