package co.redeye.core.fun;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.parser.DocumentScrape;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Sample code that finds files that
 * match the specified glob pattern.
 * For more information on what
 * constitutes a glob pattern, see
 * http://docs.oracle.com/javase/javatutorials/tutorial/essential/io/fileOps.html#glob
 *
 * The file or directories that match
 * the pattern are printed to
 * standard out.  The number of
 * matches is also printed.
 *
 * When executing this application,
 * you must put the glob pattern
 * in quotes, so the shell will not
 * expand any wild cards:
 *     java Find . -name "*.java"
 */

public class Investigate {

    //private static final String filePath = "/media/george/Seagate Backup Plus Drive/QUU First Set 150915";
    private static final String filePath = "/home/george/Downloads";

    static void usage() {
        System.err.println("java Investigate <path>");
        System.exit(-1);
    }

    public static void main(String[] args)
        throws IOException {

//        if (args.length < 1)
//            usage();

//        Path startingDir = Paths.get(args[0]);
        Path startingDir = Paths.get(filePath);

        FileStalker stalker = new FileStalker();
        Files.walkFileTree(startingDir, stalker);
        stalker.done();
    }
}
