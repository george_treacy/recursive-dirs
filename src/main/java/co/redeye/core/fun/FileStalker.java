package co.redeye.core.fun;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.parser.DocumentScrape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by george on 23/09/15.
 */
public class FileStalker extends SimpleFileVisitor<Path> {

    private static final Logger logger = LoggerFactory.getLogger(FileStalker.class);

    Integer fileCount = 0;
    Integer hasBodyCount = 0;

    // process file and increment counts.
    // the file or directory name.
    void incrementCounts(Path file) {
        Path name = file.getFileName();
        if (name != null) {
            fileCount++;
            System.out.println(file);
        }
        process(file);
    }

    private void process(final Path file) {
        DocumentScrape scrape = new DocumentScrape();
        try {
            scrape.tikaAutoParse(file.toAbsolutePath().toString());
        } catch (DocumentProcessorException e) {
            e.printStackTrace();
        }
//            System.out.println(scrape.getMetadata().names());
        if (scrape.getHandler().toString() != null && scrape.getHandler().toString().trim().length() > 100) {
            hasBodyCount++;
            System.out.println(scrape.getHandler().toString().trim().length());
        }
    }

    // Prints the total number of
    // matches to standard out.
    void done() {
        System.out.println("Total File Count: " + fileCount);
        System.out.println("Has Body Text File Count: " + hasBodyCount);
    }

    // Invoke the pattern matching
    // method on each file.
    @Override
    public FileVisitResult visitFile(Path file,
                                     BasicFileAttributes attrs) {
        incrementCounts(file);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file,
                                           IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }
}